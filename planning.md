# todo
* Add error handling for argument input 
* Add help information about argument input
* Add support for python2
* Add option to automate attack types 
    * Insert revshell
    * Insert pty
* Add support for module hijacking on Windows
