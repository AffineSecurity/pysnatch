#!/usr/bin/env python3

import os
import sys
from modulefinder import ModuleFinder
from subprocess import STDOUT, check_output


os.getuid()
os.getgid()

finder = ModuleFinder()

def introScreen():
    art = '''
__________                             __         .__     
\______   \___.__. ______ ____ _____ _/  |_  ____ |  |__  
 |     ___<   |  |/  ___//    \\\\__  \\\\   __\/ ___\|  |  \ 
 |    |    \___  |\___ \|   |  \/ __ \|  | \  \___|   Y  \\
 |____|    / ____/____  >___|  (____  /__|  \___  >___|  /
           \/         \/     \/     \/          \/     \/                            
    '''
    title = 'Pysnatch - a Python Module Hijacking script'.center(58)
    author = 'Created by Sam Ferguson'.center(58)
    handle = '@AffineSecurity'.center(58)

    print(
        f'{art}\n{title}\n{author}\n{handle}\n'
    )
    

def scriptStart():
    global target_script
    global target_path
    try:
        target_script = sys.argv[1]
    except IndexError:
        print("[ERROR] Insufficient arguments provided - please provide only the path to the target script.")
        print("Ex: ./pysnatch.py /home/affine/test.py")
        sys.exit(1)
    
    try: 
        finder.run_script(target_script)
    except FileNotFoundError: 
        print("[ERROR] Invalid file path provided - please ensure the file path provided is correct.")
        sys.exit(1)
    
    target_path = os.path.abspath(os.path.join(target_script, os.pardir))
    print(f"Target script: {target_script}\nMethods: Writeable modules, writeable PYTHONPATH\n")

def check(file):
    strings_to_avoid = ["x86_64-linux-gnu.so", "__init__", target_script]
    if not file:
        return True
    for substring in strings_to_avoid:
        if substring in file:
            return True

def poisonModuleCheck():
    print(f"Checking to see if {target_path} is writeable...")
    if os.access(target_path, os.W_OK):
        print('[EXPLOIT] Program directory is writeable!')
        print('Printing potentially exploitable modules...\n')
        for name,mod in finder.modules.items():
            file_path = mod.__file__
            res = check(file_path)
            if res:
                continue
            if "." in name:
                continue
            print(f"{name}: {file_path}")
    else:
        print("Path '{target_path}' is not writeable.")
    print("Module poisoning check has completed.")

def writeableModuleCheck():
    print("\nChecking to see if imported modules are writeable...")
    for name,mod in finder.modules.items():
        file_path = mod.__file__
        res = check(file_path)
        if res:
            continue
        if "." in name:
            continue
        if os.access(file_path, os.W_OK):
            print(f"[EXPLOIT]Module '{name}' in file path '{file_path}' is writeable!")
    print("\nWritable module check has completed")

def sudoEnvCheck():
    print("\nChecking the output of 'sudo -l' for environment variable exploits...")
    output = check_output(["sudo -l"], shell=True)
    substring = "env_keep+=PYTHONPATH"
    if substring in str(output):
        print("[EXPLOIT] The PYTHONPATH environment variable can persist through sudo usage!")
    else:
        print("No exploit found")
    print("Environment variable exploit check has completed")

def main():
    introScreen()
    scriptStart()
    poisonModuleCheck()
    writeableModuleCheck()
    sudoEnvCheck()

main()
